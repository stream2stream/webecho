<%-- 
    Document   : index
    Created on : 17-Jun-2016, 16:53:18
    Author     : Selvyn
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.celestial.dbutils.*"%>
<%@page import="java.io.IOException"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.util.logging.Logger"%>

<jsp:useBean id="globalHelper" class="com.celestial.loginmanager.ApplicationScopeHelper" scope="application"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="jquery-3.0.0.js"></script>
        <script type="text/javascript" src="userdetails_validator.js"></script>
        <title>Barclays Bank IT Foundation Case Study</title>
    </head>
    
    <body>
        <%
            String  dbStatus = "DB NOT CONNECTED";

            globalHelper.setInfo("Set any value here for application level access");
            boolean connectionStatus = globalHelper.bootstrapDBConnection();
            
            if( connectionStatus )
            {
                dbStatus = "You have successfully connected the DB server";
            }
        %>
        <h2><%= dbStatus %></h2>
        <%
            User theUser = null;
            String userName = "NOT FOUND";
            if( connectionStatus )
            {
                UserHandler uh = UserHandler.getLoader();
                try
                {
                    theUser = uh.loadFromDB(DBConnector.getConnector(), "", "99999999");
                    if( theUser != null )
                        userName = theUser.getUserID();
                } catch (IOException ex)
                {
                    Logger.getLogger("index.jsp").log(Level.SEVERE, null, ex);
                }
            }
        %>
        <h3>You are: <%=userName%></h3>

    </body>
</html>
